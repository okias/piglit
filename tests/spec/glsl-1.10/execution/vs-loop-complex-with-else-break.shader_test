# This tests a bug in the r300 compiler where it was too aggressive
# with optimizations (copy propagation) of movs in loops.
#
# See: https://gitlab.freedesktop.org/mesa/mesa/-/issues/6467
[require]
GLSL >= 1.10

[vertex shader]
uniform int loop_count;
uniform int limit;

void main()
{
  gl_Position = gl_Vertex;

  vec4 colour = vec4(1.0, 1.0, 1.0, 1.0);
  for (int i = 0; i < loop_count; i++) {

     if (i > limit) {
        colour = vec4(1.0, 0.0, 0.0, 1.0);
     }

     if (i <= limit) {
        colour = vec4(0.0, 1.0, 0.0, 1.0);
     } else {
        break;
     }
  }

  gl_FrontColor = colour;
}

[fragment shader]
void main()
{
  gl_FragColor = gl_Color;
}

[test]
clear color 0.5 0.5 0.5 0.5

uniform int limit 1
uniform int loop_count 3
draw rect -1 -1 2 2
probe all rgba 1.0 0.0 0.0 1.0

uniform int loop_count 2
draw rect -1 -1 2 2
probe all rgba 0.0 1.0 0.0 1.0

uniform int loop_count 1
draw rect -1 -1 2 2
probe all rgba 0.0 1.0 0.0 1.0

uniform int loop_count 0
draw rect -1 -1 2 2
probe all rgba 1.0 1.0 1.0 1.0
